import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from './../user.service';
import { PermissionService } from './../permission.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService,
    private permissionService: PermissionService
  ) { }

  ngOnInit() {
  }

  public logout() {
    this.userService.logOut();
    this.permissionService.permissionsChanged();
    this.router.navigate(['login']);
  }

}
