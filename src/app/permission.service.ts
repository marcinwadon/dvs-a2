import { Injectable, OnInit, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

export enum ROLE {
  GUEST,
  USER
}

interface RoleDefinition { role: ROLE, test: () => boolean };

@Injectable()
export class PermissionService implements CanActivate {

  public onPermissionsChange: EventEmitter<any> = new EventEmitter();

  private definitions: Array<RoleDefinition> = [];

  constructor(
    private router: Router
  ) { }

  public addRole(role: ROLE, testMethod: () => boolean) {
    this.definitions.push({
      role,
      test: testMethod
    });
  }

  public hasPermission(requestedRoles: Array<ROLE>): boolean {
    return this.definitions
      .filter(
        (definition: RoleDefinition): boolean => requestedRoles.indexOf(definition.role) >= 0
      )
      .map(
        (definition: RoleDefinition): boolean => definition.test.call(this)
      )
      .reduce(
        (prev: boolean, current: boolean): boolean => prev || current
      );
  }

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean|Observable<boolean>|Promise<boolean> {
    if (undefined === route.data['only']) {
      return true;
    }

    let routeRoles: Array<ROLE> = route.data['only'] as Array<ROLE>;
    let canActivate: boolean = this.hasPermission(routeRoles);

    if (!canActivate && undefined !== route.data['redirectTo']) {
      this.router.navigate([route.data['redirectTo'] as string]);
    }

    return canActivate;
  }

  public permissionsChanged(): void {
    this.onPermissionsChange.emit();
  }
}
