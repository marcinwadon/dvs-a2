import { Directive, TemplateRef, ViewContainerRef, Input, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { ROLE, PermissionService } from './permission.service';

@Directive({
  selector: '[ifRole]'
})
export class IfRoleDirective implements OnInit, OnDestroy {

  @Input('ifRole')
  public roles: Array<ROLE>;

  private hasView: boolean = false;
  private permissionsChangeSubscription: Subscription = null;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private permissionService: PermissionService
  ) {
    this.permissionsChangeSubscription = this.permissionService.onPermissionsChange.subscribe((): void => {
        this.updateViewVisibility();
      });
  }

  ngOnInit() {
    this.updateViewVisibility();
  }

  ngOnDestroy() {
    this.permissionsChangeSubscription.unsubscribe();
  }

  private updateViewVisibility(): void {
    let hasPermission = this.permissionService.hasPermission(this.roles);

    if (this.hasView === hasPermission) {
      return;
    }

    if (hasPermission) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }

    this.hasView = hasPermission;
  }
}
