import { Component } from '@angular/core';
import { ROLE } from './permission.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app works!';

  public ROLE = ROLE;
}
